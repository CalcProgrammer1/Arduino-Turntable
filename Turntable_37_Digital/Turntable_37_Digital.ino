#define OFFSET_180_DEG    3996 //4000
#define OFFSET_TRACK_1    601
#define OFFSET_TRACK_2    932 //328
#define OFFSET_TRACK_3    1156 //554
#define OFFSET_TRACK_4    1382 //778
#define OFFSET_TRACK_5    1648
#define OFFSET_TRACK_6    1904
#define KNOB_COUNTS_PER_CLICK 4
#define NUM_TRACKS        6
#define NUM_POSITIONS     (2*NUM_TRACKS)
#define ENABLE_PIN       A0  // LOW = driver enabled
#define M_1_PIN          A1  // M1 microstepping mode
#define M_2_PIN          A2  // M2 microstepping mode
#define DIR_PIN          A3  // to DIR pin of driver
#define STEP_PIN         A4  // to DIR Pin of driver
#define POTM_PIN         A5  // used to change speed
#define ROTARY_1_PIN      2  // LOW = Select Position CW
#define ROTARY_2_PIN      3  // LOW = Select Position CCW
#define SR_LATCH_PIN     11
#define SR_CLOCK_PIN     10
#define SR_DATA_PIN      12
#define ZERO_PIN         4  // LOW = start zero find routine
#define MODE_PIN          6  // HOLD Button to change mode to calibration
#define ZERO_FOUND_PIN    7  // Low = zero switch found
#define ONOFF_LED_PIN     5  // Motor running LED
#define ZEROFOUND_LED_PIN 13  // Zero Found LED
#define PULSES_PER_REV  (8 * 200)  // Pulses per revolution for this motor
#define GOTO_A_PIN        8  // Go to A side of turntable
#define GOTO_B_PIN        9  // Go to B side of turntable

// define number of positions and steps from zero to each
unsigned long tt_position[NUM_POSITIONS + 1] =
{ 0,
  OFFSET_TRACK_1,
  OFFSET_TRACK_2,
  OFFSET_TRACK_3,
  OFFSET_TRACK_4,
  OFFSET_TRACK_5,
  OFFSET_TRACK_6,
  OFFSET_TRACK_1 + 3999, //OFFSET_180_DEG,
  OFFSET_TRACK_2 + 4002, //OFFSET_180_DEG,
  OFFSET_TRACK_3 + 4000, //OFFSET_180_DEG,
  OFFSET_TRACK_4 + 4001, //OFFSET_180_DEG,
  OFFSET_TRACK_5 + 3998, //OFFSET_180_DEG,
  OFFSET_TRACK_6 + 3993 //OFFSET_180_DEG
};

byte led_state = 0;
byte dir, current_pos, new_pos, rpm, rpm_old;
unsigned long timeoflaststep;
long absoluteposition;
byte manual_mode = 0;
byte prev_mode_pin_val = 0;
 
#include <EEPROM.h>
#include <Encoder.h>

Encoder knob(ROTARY_1_PIN, ROTARY_2_PIN);

long currentknobpos = 0;
long manualknobpos = 0;

void motor_enable()  {
  digitalWrite(DIR_PIN,       dir);
  digitalWrite(ONOFF_LED_PIN, HIGH);
  digitalWrite(ENABLE_PIN,    LOW); // Low = driver enabled
}

void motor_idle() {
  // digitalWrite(ENABLE_PIN,    HIGH);// HIGH = driver disabled
  digitalWrite(ONOFF_LED_PIN,  LOW);// LOW = Motor always on when stopped
}

void find_zero() {
  Serial.println("Finding zero sensor");
  digitalWrite(ZEROFOUND_LED_PIN,   LOW);
  dir = 0;
  motor_enable();
  while (digitalRead(ZERO_FOUND_PIN) == HIGH) {
    if ((micros() - timeoflaststep) > stepintervalzero()) {
      timeoflaststep = micros();
      digitalWrite(STEP_PIN,  HIGH);
      //delayMicroseconds(20);  // only needed if step pulse is too short to be detected8888
      digitalWrite(STEP_PIN,  LOW);
    }
  }
  motor_idle();
  digitalWrite(ZEROFOUND_LED_PIN,   HIGH);
  Serial.println("Zero Sensor Found");
  Serial.println();
  current_pos = 0;
  new_pos = 0;
  absoluteposition = 0;
}

unsigned long stepinterval()  { // calculates step timing based on potmeter input
  rpm = map(analogRead(POTM_PIN), 0, 1024 , 1 , 21) * 8; // max 200 rpm, else pulses get
  if (rpm != rpm_old) {
    Serial.print("RPM: ");
    Serial.print(rpm);
    rpm_old = rpm;
  }
  return 60000000UL / PULSES_PER_REV / rpm;
}

unsigned long stepintervalzero()  { // calculates step timing based on potmeter input
  rpm = map(analogRead(POTM_PIN), 0, 1024 , 1 , 21) * 2; // max 200 rpm, else pulses get
  if (rpm != rpm_old) {
    Serial.print("RPM: ");
    Serial.print(rpm);
    rpm_old = rpm;
  }
  return 60000000UL / PULSES_PER_REV / rpm;
}
void rotate(unsigned int numsteps) {
  Serial.print("moving to pos ");
  Serial.print(new_pos);
  Serial.print("absolute position ");
  Serial.print(absoluteposition);
  Serial.print(" pulses ");
  Serial.print(numsteps);
  Serial.print(" dir ");
  Serial.print(dir);
  Serial.print("\r\n");
  motor_enable();
  while (numsteps > 0)  {
    if ((micros() - timeoflaststep) > stepinterval()) {
      timeoflaststep = micros();
      digitalWrite(STEP_PIN, HIGH);
      //delayMicroseconds(20); //only needed if step pulse is too short to be detected
      digitalWrite(STEP_PIN, LOW);
      numsteps--;
      if (dir == 1) {
        absoluteposition ++;
      }
      else absoluteposition --;
    }
  }
  motor_idle();
  Serial.print("pos ");
  Serial.print(new_pos);
  Serial.print(" reached");
  Serial.print("\r\n");
}

void setup()
{
  //Initialize pins
  pinMode(ZERO_PIN,         INPUT_PULLUP);
  pinMode(ZERO_FOUND_PIN,   INPUT_PULLUP);
  pinMode(GOTO_A_PIN,       INPUT_PULLUP);
  pinMode(GOTO_B_PIN,       INPUT_PULLUP);
  pinMode(MODE_PIN,         INPUT_PULLUP);
  pinMode(M_1_PIN,          OUTPUT);
  pinMode(M_2_PIN,          OUTPUT);
  pinMode(DIR_PIN,          OUTPUT);
  pinMode(STEP_PIN,         OUTPUT);
  pinMode(ENABLE_PIN,       OUTPUT);
  pinMode(SR_LATCH_PIN,     OUTPUT);
  pinMode(SR_CLOCK_PIN,     OUTPUT);
  pinMode(SR_DATA_PIN,      OUTPUT);
  pinMode(ONOFF_LED_PIN,    OUTPUT);
  pinMode(ZEROFOUND_LED_PIN, OUTPUT);

  //Enable microstepping
  digitalWrite(M_1_PIN,   HIGH); // SET M1, M2 to 0,0 for no microstepping
  digitalWrite(M_2_PIN,   HIGH); // SET M1, M2 to 0,0 for no microstepping

  //Initialize the motor to idle
  motor_idle();

  current_pos = EEPROM.read(0);

  if (current_pos > NUM_POSITIONS)
  {
    current_pos = 1;
    EEPROM.write(0, 1);
  }
  new_pos = current_pos;

  Serial.begin(9600);
  Serial.println();
  Serial.print("Current memory position = ");
  Serial.println(current_pos);
  Serial.println();
  Serial.println("If memory position is not according");
  Serial.println("to reality, then find zero first!");
  Serial.println();
}

long prevknobpos = 0;

void loop()
{
  stepinterval(); // read speed and show rpm on serial monitor

  digitalWrite(SR_LATCH_PIN, LOW);
  shiftOut(SR_DATA_PIN, SR_CLOCK_PIN, MSBFIRST, led_state);
  digitalWrite(SR_LATCH_PIN, HIGH);
  
  //If zero switch is pressed, initialize zero position
  if (digitalRead(ZERO_PIN) == 0)
  {
    find_zero();
  }

  byte mode_pin_val = digitalRead(MODE_PIN);
  
  //Manual override mode if mode switch pressed
  if(mode_pin_val == 0 && (mode_pin_val != prev_mode_pin_val))
  {
    if(manual_mode == 0)
    {
      manual_mode = 1;
      prevknobpos = 0;
      knob.write(0);
    }
    else
    {
      manual_mode = 0;
      knob.write(currentknobpos * KNOB_COUNTS_PER_CLICK);
    }
  }

  prev_mode_pin_val = mode_pin_val;
  
  if(manual_mode == 1)
  {
    led_state = 0xFF;
    
    manualknobpos = knob.read();

    //Check if knob position change since previous cycle was positive or negative
    if ((manualknobpos - prevknobpos) > 0)
    {
      dir = 1;
    }
    else
    {
      dir = 0;
    }

    //Rotate number of knob counts since previous cycle
    rotate(abs(manualknobpos - prevknobpos));

    prevknobpos = manualknobpos;
  }
  else
  {
    //Count clicks of knob, not encoder counts
    currentknobpos = knob.read() / KNOB_COUNTS_PER_CLICK;

    //If knob position goes out of range, put it back in range
    if (currentknobpos >= NUM_TRACKS) {
      currentknobpos = (NUM_TRACKS - 1);
      knob.write ((NUM_TRACKS - 1) * KNOB_COUNTS_PER_CLICK);
    }
    if (currentknobpos < 0) {
      currentknobpos = 0;
      knob.write (0);
    }

    //Light the LED for the selected track
    led_state = (2 << currentknobpos);

    //If either goto button is pressed, update new position
    //new_pos is knob position + 1 because new_pos starts at 1
    if (digitalRead(GOTO_A_PIN)  == 0)
    {
      new_pos = currentknobpos + 1;
    }

    //new_pos is knob position + 1, but then add NUM_TRACKS for 180 degree offset positions
    if (digitalRead(GOTO_B_PIN)  == 0)
    {
      new_pos = currentknobpos + 1 + NUM_TRACKS;
    }
  }

  //if position has changed, rotate to new position
  if (new_pos != current_pos)
  {
    if (new_pos > current_pos)
    {
      dir = 1;
      rotate(tt_position[new_pos] - tt_position[current_pos]);
    }
    else
    {
      dir = 0;
      rotate(tt_position[current_pos] - tt_position[new_pos]);
    }

    current_pos = new_pos;
    EEPROM.write(0, current_pos);
  }
}
